package com.fei.dao;

import com.fei.pojo.User;

public interface UserMapper {
    //查询一个用户
    User selectUser(int id);
}
