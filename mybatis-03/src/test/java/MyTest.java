import com.fei.dao.UserMapper;
import com.fei.pojo.User;
import com.fei.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

public class MyTest {

    @Test
    public void selectUser() {
        SqlSession session = MybatisUtils.getSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        User user = mapper.selectUser(4);
        System.out.println(user);

    }

}
