import com.fei.dao.UserMapper;
import com.fei.pojo.User;
import com.fei.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.Test;

public class Log4jMytest {
    static Logger logger = Logger.getLogger(Log4jMytest.class);

    @Test
    public void selectUser() {
        logger.info("info：进入selectUser方法");
        logger.debug("debug：进入selectUser方法");
        logger.error("error: 进入selectUser方法");
        SqlSession session = MybatisUtils.getSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        User user = mapper.selectUser(2);
        System.out.println(user);

        session.close();
    }
}
