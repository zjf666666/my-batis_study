package com.fei.dao;

import com.fei.pojo.Student;
import com.fei.pojo.Teacher;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TeacherMapper {
    @Select("select *from teacher")
    List<Teacher> getListTeacher();

    List<Teacher> selectTeacher();
}
