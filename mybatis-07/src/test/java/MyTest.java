import com.fei.dao.TeacherMapper;
import com.fei.pojo.Teacher;
import com.fei.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class MyTest {
    @Test
    public void TestDemo01() {
        SqlSession session = MybatisUtils.getSession();
        TeacherMapper mapper = session.getMapper(TeacherMapper.class);
        List<Teacher> listTeacher = mapper.getListTeacher();
        for (Teacher teacher : listTeacher) {
            System.out.println(teacher);
        }

    }

    @Test
    public void selectTeacher() {
        SqlSession session = MybatisUtils.getSession();
        TeacherMapper mapper = session.getMapper(TeacherMapper.class);
        List<Teacher> teachers = mapper.selectTeacher();
        for (Teacher teacher : teachers) {
            System.out.println(teacher);
        }
    }
}
