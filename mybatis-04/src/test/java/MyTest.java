import com.fei.dao.UserMapper;
import com.fei.pojo.User;
import com.fei.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTest {

    @Test
    public void limitUser() {

        SqlSession session = MybatisUtils.getSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("startIndex", 0);
        map.put("pageSize", 5);
        List<User> list = mapper.limitUser(map);
        for (User user : list) {
            System.out.println(user);
        }
    }
}
