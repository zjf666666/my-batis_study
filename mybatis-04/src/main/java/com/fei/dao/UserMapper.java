package com.fei.dao;

import com.fei.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    //查询一个用户
    List<User> limitUser(Map<String, Integer> map);
}
