import com.fei.dao.StudentMapper;
import com.fei.dao.TeacherMapper;
import com.fei.pojo.Student;
import com.fei.pojo.Teacher;
import com.fei.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class Mytest {
    @Test
    public void getListTeacher() {
        SqlSession session = MybatisUtils.getSession();
        TeacherMapper mapper = session.getMapper(TeacherMapper.class);

        List<Teacher> listTeacher = mapper.getListTeacher();
        for (Teacher teacher : listTeacher) {
            System.out.println(teacher);
        }
    }

    @Test
    public void getListStudent() {
        SqlSession session = MybatisUtils.getSession();
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        List<Student> listStudent = mapper.getListStudent();
        for (Student student : listStudent) {
            System.out.println(student);
        }
    }

    @Test
    public void getListStudent2() {
        SqlSession session = MybatisUtils.getSession();
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        List<Student> listStudent2 = mapper.getListStudent2();
        for (Student student : listStudent2) {
            System.out.println(student);
        }
    }
}
