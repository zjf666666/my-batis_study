package com.fei.dao;


import com.fei.pojo.Student;

import java.util.List;

public interface StudentMapper {

    List<Student> getListStudent();

    List<Student> getListStudent2();
}
