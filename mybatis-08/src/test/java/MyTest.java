import com.fei.dao.BlogMapper;
import com.fei.pojo.Blog;
import com.fei.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTest {

    @Test
    public void selectBlog() {
        SqlSession session = MybatisUtils.getSession();
        BlogMapper mapper = session.getMapper(BlogMapper.class);
        List<Blog> blogs = mapper.selectBlog();
        for (Blog blog : blogs) {
            System.out.println(blog);
        }
    }

    @Test
    public void getBlog() {
        SqlSession session = MybatisUtils.getSession();
        BlogMapper mapper = session.getMapper(BlogMapper.class);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("author", "狂神说");
        map.put("title", "微服务");
        List<Blog> blog = mapper.getBlog(map);
        for (Blog blog1 : blog) {
            System.out.println(blog1);
        }
        session.close();
    }

    @Test
    public void updateBlog() {
        SqlSession session = MybatisUtils.getSession();
        BlogMapper mapper = session.getMapper(BlogMapper.class);
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("title", "无敌是多么寂寞");
        map.put("author", "邓超");
        map.put("id", 1);
        int i = mapper.updateBlog(map);
        if (i > 0) {
            System.out.println("修改成功！！！");
        }
        session.close();
    }

    @Test
    public void queryBlog() {
        SqlSession session = MybatisUtils.getSession();
        BlogMapper mapper = session.getMapper(BlogMapper.class);
        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("author", "狂神说");
//        map.put("title", "微服务");
        map.put("id", 1);
        List<Blog> blog = mapper.queryBlog(map);
        for (Blog blog1 : blog) {
            System.out.println(blog1);
        }
        session.close();
    }

    @Test
    public void queryBlogForeach() {
        SqlSession session = MybatisUtils.getSession();
        BlogMapper mapper = session.getMapper(BlogMapper.class);
        HashMap map = new HashMap();
        List<Integer> ids = new ArrayList<Integer>();
        ids.add(1);
//        ids.add(2);
//        ids.add(3);

        map.put("ids", ids);
        List<Blog> blogs = mapper.queryBlogForeach(map);
        for (Blog blog : blogs) {
            System.out.println(blog);
        }
    }
}
