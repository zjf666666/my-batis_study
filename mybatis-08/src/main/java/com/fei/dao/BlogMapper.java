package com.fei.dao;

import com.fei.pojo.Blog;

import java.util.List;
import java.util.Map;

public interface BlogMapper {

    List<Blog> selectBlog();

    List<Blog> getBlog(Map map);

    int updateBlog(Map map);

    List<Blog> queryBlog(Map map);

    List<Blog> queryBlogForeach(Map map);
}
