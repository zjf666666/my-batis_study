package com.fei.dao;

import com.fei.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserMapper {

    //查询
    @Select("select *from user")
    List<User> selectUserList();

    //增加一个用户
    @Insert("insert into user (`id`,`name`,`pwd`) values (#{id},#{name},#{pwd})")
    int addUser(User user);

    //修改一个用户
    @Update("update user set pwd=#{pwd},name=#{name} where id=#{id}")
    int updateUser(User user);

    //删除指定用户
    @Delete("delete from user where id=#{id}")
    int deleteUser(@Param("id") int id);


}
