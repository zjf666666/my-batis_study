package com.fei.dao;

import com.fei.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    //查询所有用户
    List<User> selectUser();

    //查询
    List<User> selectUser02(String value);

    //增加一个用户
    int addUser(User user);

    //用map插入
    int addUser01(Map<String, Object> map);


    //修改一个用户
    int updateUser(User user);

    //删除一个用户
    int deleteUser(int id);


}
