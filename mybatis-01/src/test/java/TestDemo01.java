import com.fei.dao.UserMapper;
import com.fei.pojo.User;
import com.fei.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestDemo01 {

    @Test
    public void selectUser() {
        SqlSession sqlSession = MybatisUtils.getSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> list = mapper.selectUser();
        for (User user : list) {
            System.out.println(user);
        }
        sqlSession.close();
    }

    @Test
    public void selectUser02() {
        SqlSession sqlSession = MybatisUtils.getSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        String value = "%小%";
        List<User> list = mapper.selectUser02(value);
        for (User user : list) {
            System.out.println(user);
        }
        sqlSession.close();

    }

    @Test
    public void addUser() {
        SqlSession session = MybatisUtils.getSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        int res = mapper.addUser(new User(8, "六师弟", "12343"));
        if (res > 0) {
            System.out.println("插入成功！！！！");
        }

        //提交事务
        session.commit();
        session.close();
    }

    @Test
    public void addUser01() {
        SqlSession session = MybatisUtils.getSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        Map<String, Object> map = new HashMap<String, Object>();
        User user = new User(8, "王德法", "jdhssd");
        map.put("userId", user.getId());
        map.put("userName", user.getName());
        map.put("userPwd", user.getPwd());

        int i = mapper.addUser01(map);
        if (i > 0) {
            System.out.println("插入成功！！！！");
        }
        //提交事务
        session.commit();
        session.close();
    }

    @Test
    public void updateUser() {
        SqlSession session = MybatisUtils.getSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        int res = mapper.updateUser(new User(8, "李四", "123456"));
        if (res > 0) {
            System.out.println("修改成功！！！！");
        }

        //提交事务
        session.commit();
        //关闭
        session.close();
    }

    @Test
    public void deleteUser() {
        SqlSession session = MybatisUtils.getSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        int res = mapper.deleteUser(8);
        if (res > 0) {
            System.out.println("删除成功！！！！");
        }

        //提交事务
        session.commit();
        //关闭
        session.close();
    }


}
